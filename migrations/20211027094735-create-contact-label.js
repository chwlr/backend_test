'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('contact_label', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      contact_id: {
        type: Sequelize.INTEGER
      },
      label_id: {
        type: Sequelize.INTEGER
      }
    }).then(() => {
      return queryInterface.addConstraint('contact_label', {
        fields: ['contact_id'],
        type: 'foreign key',
        name: 'fk_contact_id',
        references: {
          table: 'contact',
          field: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'no action'
      }) 
    }).then(() => {
      return queryInterface.addConstraint('contact_label', {
        fields: ['label_id'],
        type: 'foreign key',
        name: 'fk_label_id',
        references: {
          table: 'label',
          field: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'no action'
      }) 
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('contact_label');
  }
};
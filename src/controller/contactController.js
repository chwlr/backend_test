
const {getContact, insertContact} = require('../model/contact')

exports.contactIndex = async (req, res) => {
  const { filter, email, phone, notes } = req.query
  const data = await getContact({ filter, email, phone, notes })
  res.send(data)
}

exports.contactCreate = async (req, res) => {
  const { name, email, phone, notes, labelName, labelSlug } = req.body

  if (name === '') {
      return res.status(400).json({
        success: false,
        message: 'name is required',
        error: 'bad request'
      })
  }

  if (email === '') {
      return res.status(400).json({
        success: false,
        message: 'email is required',
        error: 'bad request'
      })
  }

  if (phone === '') {
      return res.status(400).json({
        success: false,
        message: 'name is required',
        error: 'bad request'
      })
  }

  if (notes === '') {
      return res.status(400).json({
        success: false,
        message: 'notes is required',
        error: 'bad request'
      })
  }

  if (labelName === '') {
      return res.status(400).json({
        success: false,
        message: 'label name is required',
        error: 'bad request'
      })
  }

  if (labelSlug === '') {
      return res.status(400).json({
        success: false,
        message: 'label slug is required',
        error: 'bad request'
      })
  }

  
  try {
    const { data } = await insertContact({ name, email, phone, notes, labelSlug, labelName })
    if (data) {
        res.status(200).send(data)
    } else {
        res.status(400).send({
          success: false,
          error: 'internal server error',
          message: 'internal server error',
          data
        })
    }
  } catch (e) {
    console.error(e)
      res.status(400).send({
        success: false,
        error: 'internal server error',
        message: e.message
      })
  }
}
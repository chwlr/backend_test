const express = require('express')
const bodyParser = require('body-parser')


const app = new express()
app.disable('x-powered-by')
app.set('host', process.env.APP_HOST || '127.0.0.1')
app.set('port', process.env.APP_PORT || 8080)
app.set('app_url', `http://${app.get('host')}:${app.get('port')}`)
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true,
}))


app.use((req, res, next) => {
  res.setHeader("Content-Type", "application/json");
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

app.use(require('./router'))

module.exports = app
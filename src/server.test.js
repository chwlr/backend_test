const request = require('supertest')
const server = require('./server')

describe('contact endpoint', () => {
  it('GET /contacts --> array contact', () => {
    return request(server)
      .get('/contacts')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),
              name: expect.any(String),
              email: expect.any(String),
              phone: expect.any(String),
              notes: expect.any(String),
              created_at: expect.any(String),
              slug: expect.any(String),
            })
          ])
        )
      })
  }),

  it('GET /contacts/?filter=slug --> array contact', () => {
    return request(server)
      .get('/contacts/?filter=slug')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),
              name: expect.any(String),
              email: expect.any(String),
              phone: expect.any(String),
              notes: expect.any(String),
              created_at: expect.any(String),
              slug: expect.any(String),
            })
          ])
        )
      })
  }),

  it('GET /contacts/?filter=name --> array contact', () => {
    return request(server)
      .get('/contacts/?filter=name')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),
              name: expect.any(String),
              email: expect.any(String),
              phone: expect.any(String),
              notes: expect.any(String),
              created_at: expect.any(String),
              slug: expect.any(String),
            })
          ])
        )
      })
  }),

  it('GET /contacts/?email=chris@mail.com --> array contact', () => {
    return request(server)
      .get('/contacts/?email=chris@mail.com')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),
              name: expect.any(String),
              email: expect.any(String),
              phone: expect.any(String),
              notes: expect.any(String),
              created_at: expect.any(String),
              slug: expect.any(String),
            })
          ])
        )
      })
  })

  it('GET /contacts/?email=chris@mail.com --> array contact', () => {
    return request(server)
      .get('/contacts/?email=chris@mail.com')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),
              name: expect.any(String),
              email: expect.any(String),
              phone: expect.any(String),
              notes: expect.any(String),
              created_at: expect.any(String),
              slug: expect.any(String),
            })
          ])
        )
      })
  })

  it('GET /contacts/?phone=888 --> array contact', () => {
    return request(server)
      .get('/contacts/?phone=888')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),
              name: expect.any(String),
              email: expect.any(String),
              phone: expect.any(String),
              notes: expect.any(String),
              created_at: expect.any(String),
              slug: expect.any(String),
            })
          ])
        )
      })
  })

  it('GET /contacts/?notes=chris notes --> array contact', () => {
    return request(server)
      .get('/contacts/?notes=chris notes')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),
              name: expect.any(String),
              email: expect.any(String),
              phone: expect.any(String),
              notes: expect.any(String),
              created_at: expect.any(String),
              slug: expect.any(String),
            })
          ])
        )
      })
  })

  it('POST /contacts --> array contact', () => {
    return request(server)
      .post('/contacts')
      .send({
        name: "test input",
        email: "test@mail.com",
        phone: "1122",
        notes: "test notes",
        created_at: "NOW()",
        labelName: "test label",
        labelSlug: "test label"
      })
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual(
            expect.arrayContaining([
              expect.objectContaining({
                name: "test input",
                email: "test@mail.com",
                phone: "1122",
                notes: "test notes"
              })
            ])
        )
      })
  })
  
})

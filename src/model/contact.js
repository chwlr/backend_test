'use strict'

//import connection
const db = require('../common/database')
const { QueryTypes } = require('sequelize')

async function getContact({ filter, email, phone, notes }){
  let data, combineQuery
  let orderBynewst = `order by a.created_at desc`
  let orderByname = `order by a.name desc`
  let orderByslug = `order by label.slug asc`
  let queryWhereEmail = `and a.email like`
  let queryWherePhone = `and a.phone like `
  let queryWhereNotes = `and a.notes like `
  let query = `SELECT distinct a.id, a.name, a.email, a.phone, a.notes, a.created_at, label.name, label.slug FROM contact as a inner JOIN contact_label on contact_id = contact_label.contact_id inner JOIN label ON label.id = contact_label.label_id where a.id = label.id`

  if(filter == 'name'){
    combineQuery = `${query} ${orderByname}`
  } else if(filter == 'slug'){
    combineQuery = `${query} ${orderByslug}`
  } else if(email) {
    combineQuery = `${query} ${queryWhereEmail} '%${email}%'`
  } else if(phone) {
    combineQuery = `${query} ${queryWherePhone} '%${phone}%'`
  } else if(notes) {
    combineQuery = `${query} ${queryWhereNotes} '%${notes}%'`
  } else {
    combineQuery = `${query} ${orderBynewst}`
  }
  

  try {
    data = await db.query(combineQuery, { type: QueryTypes.SELECT })
  } catch(e) {
    console.error(e)
  }

  return data

}

async function insertContact({ name, email, phone, notes, labelName, labelSlug }){
  let lastInsertedIdContact, lastInsertedIdLabel, data
  try {
    const queryContact = `INSERT INTO contact(name, email, phone, notes, created_at) VALUES('${name}', '${email}', '${phone}', '${notes}', NOW()) returning id`
    lastInsertedIdContact = await db.query(queryContact, { type: QueryTypes.INSERT, raw: true })

    const queryLabel = `INSERT INTO label(name, slug) VALUES('${labelName}', '${labelSlug}') returning id`
    lastInsertedIdLabel = await db.query(queryLabel, { type: QueryTypes.INSERT, raw: true })

    const queryContactLabel = `INSERT INTO contact_label (contact_id, label_id) VALUES(${lastInsertedIdContact[0][0].id}, ${lastInsertedIdLabel[0][0].id})`
    await db.query(queryContactLabel, { type: QueryTypes.INSERT })

    const queryData = `select id, name, email, phone, notes from contact where id = ${lastInsertedIdContact[0][0].id}`
    data = await db.query(queryData, { type: QueryTypes.SELECT })
    
  } catch(e){
    console.error(e)
  }

  return {data}

}


module.exports = {getContact, insertContact}
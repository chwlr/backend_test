const express = require('express')
const router = express.Router({ mergeParams: true })

router.get('/contacts', require('./controller/contactController').contactIndex)
router.post('/contacts', require('./controller/contactController').contactCreate)

module.exports = router
require('dotenv').config()

const commander = require('commander')
const logger = require('./src/common/logger')
const database = require('./src/common/database')
const app = require('./src/server')

const program = new commander.Command()

program
  .name('dev-beckend')
  .version('0.0.1')

program
  .command('serve')
  .description('Run http server')
  .action((env, options) => {
    const server = app.listen(app.get('port'), () => {
      logger.info({ message: `HTTP server is started at ${app.get('app_url')} in ${app.get('env')}` })
    })

    const shutdown = () => {
      logger.info({ message: 'Received kill signal, shutting down gracefully' })
      server.close(() => {
        logger.info({ message: 'Closed database connections' })
        database.close()

        logger.info({ message: 'Closed out remaining connections' })
        process.exit(0)
      })

      setTimeout(() => {
        console.error('Could not close connections in time, forcefully shutting down')
        process.exit(1)
      }, 10000)
    }

    process.on('SIGTERM', shutdown)
    process.on('SIGINT', shutdown)
  })

program.parse(process.argv)

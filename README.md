## Requirement Tools

- Node.js
- Sequelize
- PostgreSQL

## Before you run, make sure these things first

1. Install all updated packages by typing `npm install`
2. Create database named `db_backend_test`
3. Migrate database table and seed dummy data

## How to run on local server

1. Run local server by typing `npm run serve`
2. Local server will be run on `http://localhost:3000`

## Sequelize CLI reference
https://www.npmjs.com/package/sequelize-cli

## Migration

**Running table migration**

```
npx sequelize-cli db:migrate --url 'postgres://postgres:password@localhost:5432/db_backend_test?sslmode=disable'
```

Other articles about migrations can be read [here](https://sequelize.org/master/manual/migrations.html).
